# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from .models import Series, Event

# Model Tests
# class SeriesModelTest(TestCase):
#     def test_can_add(self):
#         birthday_party = Series(title='A Birthday Party')
#         birthday_party.save()
#         birthday_party = Series.objects.first()
#         self.assertEqual(birthday_party.title, "A Birthday Party")

class EventModelTest(TestCase):
    def test_can_add(self):
        my_birthday = Event(title='Birthday Party', name='Erin Birthday')
        my_birthday.save()
        my_birthday = Event.objects.first()
        self.assertEqual(my_birthday.name, "Erin Birthday")

#View Tests
class CatViewsTest(TestCase):
    def setUp(self):
        ebday = Event(name='Erin Birthday')
        ebday.save()
    def test_can_view(self):
        response = self.client.get("/events/1/")
        self.assertContains(response, "Erin")
    def test_can_update(self):
        response = self.client.get("/events/1/-edit/")
        self.assertContains(response, "Edit")