# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Announcement, Event

class AnnouncementInline(admin.TabularInline):
    model = Announcement

@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    inlines = [AnnouncementInline]

@admin.register(Announcement)
class AnnouncementAdmin(admin.ModelAdmin):
    pass   