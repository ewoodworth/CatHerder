from django import forms
from django.core.exceptions import ValidationError

import os
import get_from_slack
from dal import autocomplete

from .models import Series, Event, Announcement
import pdb

class SeriesForm(forms.ModelForm):
    """ Create a new Series instance."""
    description = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = Series
        exclude = ['description', 'archived']

class EventForm(forms.ModelForm):
    """ Create a new Event instance"""
    class Meta:
        model = Event
        exclude = ['rsvp_yes', 'rsvp_maybe', 'rsvp_no', 'created_by', 'start_time', 'end_time']

    def get_teammate_list():
        """ Returns a list of emojis associated with this team"""

        teammate_list = get_from_slack.teammate_list()
        return teammate_list
    ### The overwrites below aren't taking and I'm getting a form validation error 
    ### on the template (unsure if JS or django) that is preventing me from adding 
    ### events. Going to make janky workaround
    # start_time = forms.TimeField(input_formats=('%g:%i%a'))
    # end_time = forms.TimeField(input_formats=('%g:%i%a'))
    start_time = forms.CharField()
    end_time = forms.CharField()
    cohost = autocomplete.Select2ListChoiceField(
        required=False,
        choice_list=get_teammate_list,
        widget=autocomplete.ListSelect2(url='teammate-autocomplete')
    )
    google_plus_link = forms.CharField(required=False, label="G+ Link")
    business = forms.BooleanField(required=False)

class RSVPForm(forms.Form):
    """ Collect users's choice of RSVP option for a given event"""
    rsvp_value = forms.ChoiceField(choices=[('Yes', 'Yes'), 
                                            ('Maybe', 'Maybe'), 
                                            ('No', 'No')])

class AnnouncementForm(forms.Form):
    """ Draft an announcement for a given event and post in Slack."""

    def get_emoji_choice_list():
        """ Returns a list of emojis associated with this team"""

        emoji_list = get_from_slack.emoji_list()
        return emoji_list

    emoji_flourish = autocomplete.Select2ListChoiceField(
                                required=False,
                                choice_list=get_emoji_choice_list,
                                widget=autocomplete.ListSelect2(
                                                    url='emoji-list-autocomplete')
                                )

    channels_from_slack = []
    for item in get_from_slack.user_channels():
        if item['id'][0] == 'G':
            item['display_name'] = item['name'] + ' (private)'
        else:
            item['display_name'] = item['name']
        channels_from_slack.append((item['name'], item['display_name']))
    channels = forms.MultipleChoiceField(choices=channels_from_slack, 
                                         widget=forms.CheckboxSelectMultiple)

    time_notes = [('-----', '-----'),
                  ('TODAY', 'TODAY!'), 
                  ('TOMORROW', 'TOMORROW!'), 
                  ('NEXT WEEK','NEXT WEEK!')]
                  
    optional_note = forms.ChoiceField(choices=time_notes)