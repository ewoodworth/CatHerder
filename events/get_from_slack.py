import os
import json
from slacker import Slacker

def user_channels():
    """ Returns a list of channel dictionaries, each of which is like:
    {u'topic': {u'last_set': 0, u'value': u'Non-work banter and water cooler conversation', u'creator': u''}, 
    u'is_general': False, 
    u'name_normalized': u'random', 
    u'name': u'random', 
    u'is_channel': True, 
    u'created': 1497814294, 
    u'is_member': True, 
    u'is_mpim': False, 
    u'is_archived': False, 
    u'creator': u'U5VGAQ05Q', 
    u'is_org_shared': False, 
    u'previous_names': [], 
    u'num_members': 2, 
    u'purpose': {u'last_set': 0, u'value': u"A place for non-work-related flimflam, faffing, hodge-podge or jibber-jabber you'd prefer to keep out of more focused work-related channels.", u'creator': u''}, 
    u'members': [u'U5VGAQ05Q', u'U5YDB9TCN'], 
    u'id': u'C5WAV23LN', 
    u'is_private': False, 
    u'is_shared': False}"""

    slack = Slacker(os.environ['SLACK_API_TOKEN'])
    # pdb.set_trace()
    channels_list = slack.channels.list()
    channels = channels_list.body['channels']

    groups_list = slack.groups.list()
    groups = groups_list.body['groups']

    user_channels_all = channels + groups

    return user_channels_all

def user_channels_dict():
    """Returns dictionary of channel names and ids"""
    channel_dict = {}
    for channel in user_channels():
        channel_dict[channel['name']] = channel['id']
    return channel_dict

def user_channel_ids():
    user_channel_ids = [channel['id'] for channel in user_channels()]
    return user_channel_ids

def emoji_list():
    """ Returns a list of available emoji names"""

    #standard
    with open('emoji.json') as data_file:    
        data = json.load(data_file)
    emoji_list = ['----']
    for standard_emoji in data:
        emoji_list.append(standard_emoji['short_name'])

    #team custom
    slack = Slacker(os.environ['SLACK_API_TOKEN'])
    emoji_dictionary = slack.emoji.list()
    emojis = emoji_dictionary.body['emoji']
    for emoji in emojis:
        emoji_list.append(emoji)
    emoji_list.sort()

    return emoji_list

def teammate_list():
    """Returns a list of tem memebers"""
    pp = pprint.PrettyPrinter(indent=4)
    slack = Slacker(os.environ['SLACK_API_TOKEN'])
    users_dict = slack.users.list()
    teammate_list = []
    for member in users_dict.body['members']:
        teammate_list.append(member['name']) 
    # user slack id lives at member['id']
    return teammate_list