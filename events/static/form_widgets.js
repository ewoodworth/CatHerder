// Widgets to overwrite form elements throughout project. JQuery.
/**
 * Forces a reload of all stylesheets by appending a unique query string
 * to each stylesheet URL.
 */
function reloadStylesheets() {
    var queryString = '?reload=' + new Date().getTime();
    $('link[rel="stylesheet"]').each(function () {
        this.href = this.href.replace(/\?.*|$/, queryString);
    });
}
// timepicker for event create form
$('#id_start_time').timepicker({ 'scrollDefault': 'now' });
$('#id_end_time').timepicker({ 'scrollDefault': 'now' });
// datepicker for event create form
$( function() {
$('#id_date').datepicker();
} );