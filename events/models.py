from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField, ArrayField
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save
from django.dispatch import receiver

from django.db import models

### MARK OF AN AMATEUR. GET ADVICE
def rsvp_default():
    return {}

def profile_rsvp_default():
    return []

class Series(models.Model):
    """Series template for recurrent events"""
    title = models.CharField(max_length=64, blank=True)
    description = models.CharField(max_length=512, blank=True)
    archived = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'series'
        ordering = ['title']

    def __repr__(self):
        """Provide helpful representation when printed."""

        return "<Event name=%s>" %(self.title)

    def __str__(self):
        return "{}".format(self.title)

    def get_absolute_url(self):
        return reverse(
            'series_detail',
            kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse(
            'series_delete',
            kwargs={'pk':self.pk})

class Event(models.Model):
    """Event instance"""

    series = models.ForeignKey(Series, on_delete=models.CASCADE, null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    authorized_channels = ArrayField(models.CharField(max_length=10, null=True, blank=True), blank=True, default=[])
    title = models.CharField(max_length=64, blank=True)
    description = models.CharField(max_length=512, null=True, blank=True)

    start_time = models.TimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    end_time = models.TimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    date = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)

    rsvp_yes = JSONField(null=True, default=rsvp_default)
    rsvp_maybe = JSONField(null=True, default=rsvp_default)
    rsvp_no = JSONField(null=True, default=rsvp_default)

    location = models.CharField(max_length=63, blank=True)
    business = models.BooleanField(blank=True, default=False)
    address = models.CharField(max_length=95, blank=True)
    city = models.CharField(max_length=35, blank=True)
    state = models.CharField(max_length=2, blank=True)
    zip_code = models.CharField(max_length=15, blank=True)
    cohost = models.CharField(max_length=100, null=True, blank=True)

    yelp_link = models.CharField(max_length=2083, null=True, blank=True)
    google_plus_link = models.CharField(max_length=2083, null=True, blank=True)

    class Meta:
        verbose_name = 'event'
        ordering = ['date', 'start_time']

    def __repr__(self):
        """Provide helpful representation when printed."""

        return "<Event name=%s date=%s>" %(self.title, self.date)

    def __str__(self):
        return "{}: {}".format(
            self.title,
            self.description)

    def get_absolute_url(self):
        return reverse(
            'event_detail',
            kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse(
            'event_delete',
            kwargs={'pk':self.pk})

class Announcement(models.Model):
    event = models.ForeignKey(Event)
    channel = models.CharField(max_length=50, blank=True)
    time = models.DateTimeField(auto_now_add=True, null=True, blank=True)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # events_yes_janky = models.CharField(max_length=2083, blank=True, default="")
    # events_maybe_janky = models.CharField(max_length=2083, blank=True, default="")
    # events_no_janky = models.CharField(max_length=2083, blank=True, default="")
    events_yes = ArrayField(models.CharField(max_length=10, null=True, blank=True), blank=True, default=profile_rsvp_default)
    events_maybe = ArrayField(models.CharField(max_length=10, null=True, blank=True), blank=True, default=profile_rsvp_default)
    events_no = ArrayField(models.CharField(max_length=10, null=True, blank=True), blank=True, default=profile_rsvp_default)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()