from django.contrib.auth import logout
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse, HttpRequest, Http404, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views import generic
from django.views.generic.edit import FormView

from models import Series, Event, Announcement, Profile
from forms import EventForm, SeriesForm, AnnouncementForm, RSVPForm
import get_from_slack

import requests, os
from slacker import Slacker
from datetime import datetime
import time
from dal import autocomplete

#helper functions

def logout_view(request):
    """ Log user out of app, redirect to home url."""
    logout(request)
    return redirect('home')

class TeammateAutocompleteFromList(autocomplete.Select2ListView):
    """ This view will feed the widget in the Event Form"""
    
    def get_list(self):
        teammate_list = get_from_slack.teammate_list()
        return teammate_list

def get_authorized_events(self):
    user = self.request.user
    events = Event.objects.all()
    authorized_event_id_list = []
    for event in events:
        start_char = list({channel[0] for channel in event.authorized_channels})
        if event.created_by == user:
        # the logged in user created the event
            authorized_event_id_list.append(event.id)
        elif len(start_char) == 0:
        #no announcements have been made/ no authorization to structure
            pass
        elif "C" in start_char:
        #announcements have been made entirely to open channels
            authorized_event_id_list.append(event.id)
        else:
            user_channel_id_list = [channel['id'] for channel in get_from_slack.user_channels()]
            #channels the user has access to
            user_private_channel_ids = {}
            for channel in user_channel_id_list:
                if channel[0] == "G":
                    user__private_channel_ids.add(channel)
            event_channelset = {channel for channel in event.authorized_channels}
            if len(user__private_channel_ids & event_channelset) > 0:
                #user belongs to any private channels in which the announcement was made
                authorized_event_id_list.append(event.id)
    return Event.objects.filter(id__in=authorized_event_id_list)

class EmojiAutocompleteFromList(autocomplete.Select2ListView):
    """ This view will feed the widget in the Announcement Form view"""
    
    def get_list(self):
        emoji_list = get_from_slack.emoji_list()
        return emoji_list

#series views

class SeriesListView(generic.ListView):
    model = Series

class SeriesDetailView(generic.DetailView):
    model = Series

class SeriesCreateView(generic.CreateView):
    model = Series
    form_class = SeriesForm
    success_url=reverse_lazy('event_create')

class SeriesUpdateView(generic.UpdateView):
    model = Series
    fields = ['title', 'description']

class SeriesDeleteView(generic.DeleteView):
    model = Series
    success_url=reverse_lazy('series_list')

#event list views

class EventListView(generic.ListView):
    model = Event
    template = "event_list.html"

    def get_queryset(self):
        return get_authorized_events(self)

class EventAttendingListView(generic.ListView):

    def get_queryset(self):
        user = self.request.user
        profile = Profile.objects.get(user=user)
        return get_authorized_events(self).filter(id__in=profile.events_yes)


class EventUpcomingListView(generic.ListView):

    def get_queryset(self):
        now = datetime.now()
        return get_authorized_events(self).filter(date__gte=now)

class EventHostingListView(generic.ListView):

    def get_queryset(self):
        user = self.request.user
        return get_authorized_events(self).filter(created_by=user)

#event alteration views

class EventDetailView(generic.DetailView):
    model = Event

    def get_context_data(self, **kwargs):
        """ Add the event object associated with this session to the context."""

        context = super(EventDetailView, self).get_context_data(**kwargs)
        this_event = Event.objects.get(pk=self.kwargs['pk'])
        google_search_string = ""
        if this_event.business == True:
            google_search_string += this_event.location.replace(" ", "%20") + ","
        if this_event.address: google_search_string += this_event.address.replace(" ", "%20").replace(".", "")
        if this_event.address: google_search_string += this_event.city.replace(" ", "%20")
        context['google_src'] = "//www.google.com/maps/embed/v1/place?q=" + google_search_string + "&zoom=17&key=" + os.environ['GOOGLE_MAPS_KEY']
        return context

class EventCreateView(generic.CreateView):
    """Create a new event"""
    model = Event
    form_class = EventForm

    def form_valid(self, form):
        """Actions upon return of valid form."""
        ### Thus commences janky-fix. Should be able to overwrite form element to 
        ### accept 12 hour times, but couldn't figure it out See also forms.py
        start_time_string = form.cleaned_data['start_time'].upper()
        start_time = datetime.strptime(start_time_string, "%I:%M%p")
        end_time_string = form.cleaned_data['end_time'].upper()
        end_time = datetime.strptime(end_time_string, "%I:%M%p")
        ### Pause janky
        event = form.save(commit=False)
        event.created_by = self.request.user
        ### Resume janky
        event.start_time = start_time
        event.end_time = end_time
        ### End janky
        event.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return 'events'

class EventUpdateView(generic.UpdateView):
    model = Event
    form_class = EventForm

class EventDeleteView(generic.DeleteView):
    model = Event
    success_url=reverse_lazy('all_events')

#RSVP Views

def rsvp_yes(request, pk):
    #connect db with THIS event
    event = Event.objects.get(pk=pk)
    user = request.user
    #save rsvp to user profile
    profile = Profile.objects.get(user=user)
    profile.events_yes.append(event.id)
    # if event.id in profile.events_maybe: profile.events_maybe.remove(event.id)  ##Don't need to track the places you might go (YET)
    # if event.id in profile.events_no: profile.events_no.remove(event.id)        ##Don't need to track the places you aren't going (YET)
    profile.save()
    #save rsvp to event instance
    #set up Slacker
    slack = Slacker(os.environ['SLACK_API_TOKEN'])
    # get an id number from this app
    user_id = str(user.id)
    # get profile incl. name and avatar from slack
    user_profile = slack.users.profile.get()
    user_name = user_profile.body['profile']['real_name_normalized']
    #image sizes: 24, 32, 48, 72, 192, 512, 1024
    user_avatar = user_profile.body['profile']['image_24']
    event.rsvp_yes[user_id] = {'name':user_name, 'avatar':user_avatar}
    event.rsvp_no.pop(user_id, None)
    event.rsvp_maybe.pop(user_id, None)
    event.save()
    return redirect(event)

def rsvp_maybe(request, pk):
    #connect db with THIS event
    event = Event.objects.get(pk=pk)
    user = request.user
    #save rsvp to user profile
    profile = Profile.objects.get(user=user)
    new_string = str(event.id)
    if event.id in profile.events_yes: profile.events_yes.remove(event.id)
    # profile.events_maybe.append(event.id)                                  ##Don't need to track the places you might go (YET)
    # if event.id in profile.events_no: profile.events_no.remove(event.id)   ##Don't need to track the places you aren't going (YET)
    profile.save()
    #save rsvp to event instance
    #set up Slacker
    slack = Slacker(os.environ['SLACK_API_TOKEN'])
    # get an id number from this app
    user_id = str(user.id)
    # get profile incl. name and avatar from slack
    user_profile = slack.users.profile.get()
    user_name = user_profile.body['profile']['real_name_normalized']
    #image sizes: 24, 32, 48, 72, 192, 512, 1024
    user_avatar = user_profile.body['profile']['image_24']
    event.rsvp_maybe[user_id] = {'name':user_name, 'avatar':user_avatar}
    event.rsvp_no.pop(user_id, None)
    event.rsvp_yes.pop(user_id, None)
    event.save()
    return redirect(event)

def rsvp_no(request, pk):
    #connect db with THIS event
    event = Event.objects.get(pk=pk)
    user = request.user
    #save rsvp to user profile
    profile = Profile.objects.get(user=user)
    new_string = str(event.id)
    if event.id in profile.events_yes: profile.events_yes.remove(event.id)
    # if event.id in profile.events_maybe: profile.events_maybe.remove(event.id)  ## Don't need to track the places you might go (YET)
    # profile.events_no.append(event.id)                                          ## Don't need to track the places you aren't going (YET)
    profile.save()
    #save rsvp to event instance
    #set up Slacker
    slack = Slacker(os.environ['SLACK_API_TOKEN'])
    # get an id number from this app
    user_id = str(user.id)
    # get profile incl. name and avatar from slack
    user_profile = slack.users.profile.get()
    user_name = user_profile.body['profile']['real_name_normalized']
    #image sizes: 24, 32, 48, 72, 192, 512, 1024
    user_avatar = user_profile.body['profile']['image_24']
    #save changes to rsvp columns of event instance
    event.rsvp_no[user_id] = {'name':user_name, 'avatar':user_avatar}
    event.rsvp_yes.pop(user_id, None)
    event.rsvp_maybe.pop(user_id, None)
    event.save()
    return redirect(event)

#announcement views
class AnnouncementCreateView(generic.FormView):
    """Create an announcement in Slack for a particular event, across one or 
    more channels"""

    form_class = AnnouncementForm
    template_name='events/announcement_form.html'


    def get_context_data(self, **kwargs):
        """ Add the event object associated with this session to the context."""

        context = super(AnnouncementCreateView, self).get_context_data(**kwargs)
        this_event = Event.objects.get(pk=self.kwargs['pk'])
        context ['event'] = this_event
        context['announcements'] = Announcement.objects.filter(event=this_event)
        return context

    def form_valid(self, form):
        """Actions performed with data returned fro POST resquest. 
        Will take form details and post an announcement to Slack."""

        slack = Slacker(os.environ['SLACK_API_TOKEN'])

        event = Event.objects.get(pk=self.kwargs['pk'])
        emoji_flourish = form.cleaned_data['emoji_flourish']
        optional_note = form.cleaned_data['optional_note']
        if optional_note == '-----':
            optional_note = None
        #formatting for Slack
        if event.series: series = event.series.title.upper()
        start_time = time.strftime("%l:%M %P", 
                                   time.strptime(str(event.start_time), 
                                                 "%H:%M:%S"))
        end_time = time.strftime("%l:%M %P", 
                                 time.strptime(str(event.end_time), 
                                               "%H:%M:%S"))
        date = time.strftime("%B %e, %Y", time.strptime(str(event.date), 
                                                        "%Y-%m-%d"))

        #Announcement drafting. Best way to conditionally include the pieces?
        announcement_content = ""
        if emoji_flourish: announcement_content += (':'+ emoji_flourish +': ') * 14
        if optional_note: announcement_content += '\n'+ optional_note + '\n'
        announcement_content += '\n'+ date
        if end_time: 
            announcement_content += '\n' + 'from ' + start_time + ' to ' + end_time
        else:
            announcement_content += '\n' + 'at ' + start_time
        if event.series: 
            announcement_content += '\n\n' + series + ' ' + event.title
        else:
            announcement_content += '\n\n' + event.title
        if event.location: announcement_content += '\n' + event.location
        if event.yelp_link: announcement_content += '\n' + 'Yelp: ' + event.yelp_link
        if emoji_flourish: announcement_content += '\n' + (':'+ emoji_flourish +': ') * 14

        #Post one announcement per channel, and record the announcement instance in the db
        id_lookup = get_from_slack.user_channels_dict()
        for channel in form.cleaned_data['channels']:
            slack.chat.post_message('#' + channel, 
                                    announcement_content, 
                                    as_user=True)
            event.authorized_channels.append(id_lookup[channel])
            event.save()
            ann = Announcement(event=event, channel=channel)
            ann.save()

        return super(AnnouncementCreateView, self).form_valid(form)

    def get_success_url(self):
        return Event.objects.get(pk=self.kwargs['pk']).get_absolute_url()